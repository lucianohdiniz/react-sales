import React from "react";
import { CssBaseline, Grid, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    width: "100%",
    height: "100vh",
  },
});

function Index(props: React.PropsWithChildren<any>) {
  const classes = useStyles();

  return (
    <div>
      <CssBaseline />
      <Grid container className={classes.root}>
        {props.children}
      </Grid>
    </div>
  );
}

export default Index;
