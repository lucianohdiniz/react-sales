import React from "react";
import { Container, CssBaseline, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    paddingTop: 100,
  },
});

interface BaseTemplateProps {
  nav: React.ReactNode;
  children: React.ReactNode;
}

function Index(props: BaseTemplateProps) {
  const classes = useStyles();

  return (
    <div>
      <CssBaseline />
      {props.nav}
      <Container className={classes.root}>{props.children}</Container>
    </div>
  );
}

export default Index;
