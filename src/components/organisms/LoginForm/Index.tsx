import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useAppDispatch } from "hooks";
import { signIn } from "store/login/login.reducer";

//material
import {
  Grid,
  Card,
  CardContent,
  CardActions,
  makeStyles,
} from "@material-ui/core";

//atoms
import Text, { TextVariant } from "components/atoms/Text/Index";
import Input from "components/atoms/Input/Index";
import Button from "components/atoms/Button/Index";
import { LoginUser } from "models/User";

const useStyles = makeStyles({
  space_top: {
    paddingTop: 15,
  },
  full_width: {
    width: "100%",
  },
});

interface LoginFormProps {}

function LoginForm(props: LoginFormProps) {
  const classes = useStyles();
  const dispatch = useAppDispatch();
  const history = useHistory();

  const [loginUser, setLoginUser] = useState<LoginUser>({
    username: "",
    password: "",
  });

  const handleInputChange = (field: string, value: string) => {
    const currentLoginUserState = { ...loginUser } as any;
    currentLoginUserState[field] = value;

    setLoginUser({ ...currentLoginUserState });
  };

  const onClickLogin = () => {
    if (loginUser.username !== "admin" || loginUser.password !== "admin") {
      alert("Invalid username/password");
    } else {
      dispatch(signIn(loginUser));
      history.push("/");
    }
  };

  return (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item xs={6}>
        <Card>
          <CardContent>
            <Text gutterBottom variant={TextVariant.H5}>
              Login
            </Text>
            <br />

            <Grid container direction="row">
              <Grid item xs={12}>
                <Input
                  className={classes.full_width}
                  onChange={(event) =>
                    handleInputChange("username", event.target.value)
                  }
                  type="text"
                  id="username"
                  label="Username"
                />
              </Grid>
              <Grid item xs={12} className={classes.space_top}>
                <Input
                  className={classes.full_width}
                  onChange={(event) =>
                    handleInputChange("password", event.target.value)
                  }
                  type="password"
                  id="password"
                  label="Password"
                />
              </Grid>
            </Grid>
          </CardContent>
          <CardActions>
            <Grid container justifyContent="flex-end" alignItems="flex-end">
              <Button
                size="large"
                color="primary"
                id="login_btn"
                text="Login"
                variant="contained"
                onClick={onClickLogin}
              />
            </Grid>
          </CardActions>
        </Card>
      </Grid>
    </Grid>
  );
}

export default LoginForm;
