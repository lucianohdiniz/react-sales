import React from "react";
import { Route, Router } from "react-router";
import { createMemoryHistory, MemoryHistory } from "history";
import {
  render,
  configure,
  fireEvent,
  RenderResult,
} from "@testing-library/react";
import "@testing-library/jest-dom";

import { Provider } from "react-redux";
import store from "store";
import LoginForm from "./Index";

let history: MemoryHistory;
let usernameInputId = "username";
let passwordInputId = "password";
let loginButtonId = "login_btn";

const changeInputValue = (input: HTMLElement, value: string) => {
  fireEvent.change(input, { target: { value } });
};

const clickButton = (button: HTMLElement) => {
  fireEvent.click(button);
};

const doLogin = async (
  dom: RenderResult,
  username: string,
  password: string
) => {
  const usernameInput = await dom.findByTestId(usernameInputId);
  changeInputValue(usernameInput, username);

  const passwordInput = await dom.findByTestId(passwordInputId);
  changeInputValue(passwordInput, password);

  const loginButton = await dom.findByTestId(loginButtonId);
  clickButton(loginButton);
};

beforeEach(() => {
  history = createMemoryHistory();
  configure({ testIdAttribute: "id" });
});

it("should render LoginForm", async () => {
  //arrange
  const dom = render(
    <Provider store={store}>
      <LoginForm />
    </Provider>
  );

  //assert
  expect(dom).toBeTruthy();
});

it("should redirect when authentication is successful", async () => {
  //arrange
  history.push("/login");

  const dom = render(
    <Provider store={store}>
      <Router history={history}>
        <Route path="/login" component={LoginForm} />
      </Router>
    </Provider>
  );

  //act
  await doLogin(dom, "admin", "admin");

  //assert
  expect(history.location.pathname).not.toBe("/login");
});

it("should emit an alert when the username and password is incorret", async () => {
  const alertMock = jest.spyOn(window, "alert").mockImplementation(() => {});

  //arrange
  const dom = render(
    <Provider store={store}>
      <LoginForm />
    </Provider>
  );

  //act
  await doLogin(dom, "incorrent", "credentials");
  expect(alertMock).toHaveBeenCalled();
});
