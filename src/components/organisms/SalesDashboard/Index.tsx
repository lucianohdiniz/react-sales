import React, { useState, useEffect } from "react";
import saleService from "services/Sale/SaleService";
import { Grid } from "@material-ui/core";

//molecules
import VerticalBarGraph, {
  VerticalBarGraphData,
} from "components/molecules/VerticalBarGraph/Index";

import _ from "lodash";
import { Sale } from "models/Sale";

interface SalesDashboardProps {}

function SalesDashboard(props: SalesDashboardProps) {
  const [salesPerYear, setSalesPerYear] = useState<VerticalBarGraphData[]>([]);
  const [salesPerProduct, setSalesPerProduct] = useState<
    VerticalBarGraphData[]
  >([]);
  const [salesPerCity, setSalesPerCity] = useState<VerticalBarGraphData[]>([]);

  const groupSalesBy = (
    prop: string,
    sales: Array<Sale>
  ): VerticalBarGraphData[] => {
    let salesGrouped = _.groupBy(sales, prop);

    return Object.keys(salesGrouped).map<VerticalBarGraphData>((key) => {
      const sum = _.sumBy(salesGrouped[key], "sales");

      return {
        x: key,
        y: sum,
      };
    });
  };

  useEffect(() => {
    saleService.getAll().then((result) => {
      //sales per year
      const salesPerYear = groupSalesBy("yeaR_ID", result);
      setSalesPerYear(salesPerYear);

      const salesPerProduct = groupSalesBy("productline", result);
      setSalesPerProduct(salesPerProduct);

      const salesPerCity = groupSalesBy("city", result);
      setSalesPerCity(salesPerCity);
    });
  }, []);

  return (
    <Grid container direction="row" spacing={3}>
      <Grid item xs={6}>
        <VerticalBarGraph graphTitle="Sales per year" data={salesPerYear} />
      </Grid>

      <Grid item xs={6}>
        <VerticalBarGraph
          graphTitle="Sales per product"
          data={salesPerProduct}
        />
      </Grid>

      <Grid item xs={6}>
        <VerticalBarGraph graphTitle="Sales per city" data={salesPerCity} />
      </Grid>
    </Grid>
  );
}

export default SalesDashboard;
