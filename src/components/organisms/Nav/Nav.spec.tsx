import React from "react";
import { render, configure, fireEvent } from "@testing-library/react";
import { createMemoryHistory, MemoryHistory } from "history";
import { Router, Route } from "react-router-dom";

import Nav from "./Index";
import { Provider } from "react-redux";
import store from "store/index";

import { signIn } from "store/login/login.reducer";

let title = "Sales App";
let logoutButtonId = "logout_btn";
let history: MemoryHistory;

const mockLoginUser = () => {
  store.dispatch(
    signIn({
      username: "test_user",
    })
  );
};

const clickButton = (button: HTMLElement) => {
  fireEvent.click(button);
};

beforeEach(() => {
  history = createMemoryHistory();
  configure({ testIdAttribute: "id" });
});

it("should render Nav with Sales App text in title", async () => {
  //arrange
  const dom = render(
    <Provider store={store}>
      <Nav />
    </Provider>
  );

  //act
  const titleElement = await dom.findByText(title);

  //assert
  expect(dom).toBeTruthy();
  expect(titleElement).toBeTruthy();
  expect(titleElement.innerHTML).toBe(title);
});

it("should render a logout button when the user is logged", async () => {
  //arrange
  mockLoginUser();

  const dom = render(
    <Provider store={store}>
      <Nav />
    </Provider>
  );

  //act
  const button = await dom.findByTestId(logoutButtonId);

  expect(button).toBeTruthy();
  expect(button).toBeVisible();
});

it("should redirect to the login when click on logout button", async () => {
  //arrange
  mockLoginUser();

  history.push("/nav");

  const dom = render(
    <Provider store={store}>
      <Router history={history}>
        <Route path="/nav" component={Nav} />
      </Router>
    </Provider>
  );

  //act
  const button = await dom.findByTestId(logoutButtonId);
  clickButton(button);

  expect(history.location.pathname).toBe("/login");
});
