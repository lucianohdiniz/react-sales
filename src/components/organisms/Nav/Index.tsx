import React from "react";
import { AppBar, Toolbar, makeStyles } from "@material-ui/core";

import { useHistory } from "react-router-dom";
import { useAppSelector, useAppDispatch } from "hooks";

import { signOut } from "store/login/login.reducer";

//atoms
import Text, { TextVariant } from "components/atoms/Text/Index";
import Button from "components/atoms/Button/Index";

const useStyles = makeStyles({
  nav_title: {
    cursor: "pointer",
    flexGrow: 1,
  },
});

interface NavProps {}

function Nav(props: NavProps) {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useAppDispatch();

  const loggedUser = useAppSelector((state) => state.login.loggedUser);

  const logOut = () => {
    dispatch(signOut());
    history.push("/login");
  };

  return (
    <AppBar position="fixed">
      <Toolbar>
        <Text
          className={classes.nav_title}
          onClick={() => {
            history.push("/");
          }}
          variant={TextVariant.H6}
        >
          Sales App
        </Text>

        {loggedUser ? (
          <Button
            id="logout_btn"
            onClick={logOut}
            color="inherit"
            text="Logout"
            size="large"
          />
        ) : (
          ""
        )}
      </Toolbar>
    </AppBar>
  );
}

export default Nav;
