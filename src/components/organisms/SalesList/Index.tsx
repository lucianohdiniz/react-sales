import React, { useState, useEffect } from "react";
import { GridColDef } from "@material-ui/data-grid";
import { Sale } from "models/Sale";

import saleService from "services/Sale/SaleService";
import DataGrid from "components/atoms/DataGrid/Index";

const columns: GridColDef[] = [
  { field: "id", headerName: "Order Number", width: 170 },
  {
    field: "quantityordered",
    headerName: "Quantity Ordered",
    width: 170,
  },
  {
    field: "priceeach",
    headerName: "Price Each",
    width: 170,
  },
  {
    field: "orderlinenumber",
    headerName: "Order Line Number",
    width: 170,
  },
  {
    field: "sales",
    headerName: "Sales",
    width: 170,
  },
  {
    field: "orderdate",
    headerName: "Order Date",
    width: 170,
  },
  {
    field: "status",
    headerName: "Status",
    width: 170,
  },
  {
    field: "customername",
    headerName: "Customer",
    width: 170,
  },
  {
    field: "state",
    headerName: "State",
    width: 170,
  },
];

interface SalesListProps {}

function SalesList(props: SalesListProps) {
  const [sales, setSales] = useState<Array<Sale>>([]);

  useEffect(() => {
    saleService.getAll().then((result) => {
      setSales(result);
    });
  }, []);

  return (
    <div style={{ height: 650, width: "100%" }}>
      <DataGrid rows={sales} columns={columns} pageSize={10} />
    </div>
  );
}

export default SalesList;
