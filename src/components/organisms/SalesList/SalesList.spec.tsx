import React from "react";
import { render, RenderResult } from "@testing-library/react";
import { act } from "react-dom/test-utils";

import SalesList from "./Index";
import salesService from "services/Sale/SaleService";
import { Sale } from "models/Sale";

const mockSalesService = () => {
  const salesMock: Sale[] = [];

  for (let i = 1; i <= 10; i++) {
    salesMock.push({
      id: i,
      quantityordered: 30,
      priceeach: 95.7,
      orderlinenumber: 2,
      sales: 2871,
      orderdate: "2/24/2003 0:00",
      status: "Shipped",
      qtR_ID: 1,
      montH_ID: 2,
      yeaR_ID: 2003,
      productline: "Motorcycles",
      msrp: 95,
      productcode: "S10_1678",
      customername: "Land of Toys Inc.",
      phone: "2125557818",
      addresslinE1: "897 Long Airport Avenue",
      addresslinE2: "",
      city: "NYC",
      state: "NY",
      postalcode: "10022",
      country: "USA",
      territory: "NA",
      contactlastname: "Yu",
      contactfirstname: "Kwai",
      dealsize: "Small",
    });
  }

  const mock = jest.spyOn(salesService, "getAll");
  mock.mockReturnValue(Promise.resolve(salesMock));
};

test("it should render SalesList", async () => {
  //arrange

  let dom: RenderResult;
  act(() => {
    mockSalesService();
    dom = render(<SalesList />);
  });

  //assert
  expect(dom).toBeTruthy();
});
