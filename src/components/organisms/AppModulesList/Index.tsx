import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {
  makeStyles,
  Grid,
  Card,
  CardContent,
  CardActionArea,
  CardMedia,
} from "@material-ui/core";

//atoms
import Text, { TextVariant } from "components/atoms/Text/Index";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
});

interface ApplicationModule {
  name: string;
  path: string;
  img: string;
}
interface AppModulesListProps {}

function AppModulesList(props: AppModulesListProps) {
  const classes = useStyles();
  const history = useHistory();

  const [appModules] = useState<Array<ApplicationModule>>([
    {
      name: "Dashboards",
      path: "/sales/dashboard",
      img: "https://image.pngaaa.com/412/4851412-middle.png",
    },
    {
      name: "Sales",
      path: "/sales",
      img: "https://www.clipartmax.com/png/middle/147-1477041_free-sales-icons-sales-icon.png",
    },
  ]);

  return (
    <Grid container spacing={2}>
      {appModules.map((item, index) => (
        <Grid key={index} item xs={4}>
          <Card
            id="card-module"
            className={classes.root}
            onClick={() => {
              history.push(item.path);
            }}
          >
            <CardActionArea>
              <CardMedia component="img" image={item.img} />
              <CardContent>
                <Text gutterBottom variant={TextVariant.H5}>
                  {item.name}
                </Text>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
}

export default AppModulesList;
