import React from "react";
import { render, configure, fireEvent } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router, Route } from "react-router-dom";

import AppModulesList from "./Index";

describe("AppModulesList", () => {
  it("Should render AppModulesList", async () => {
    //arrange
    const dom = render(<AppModulesList />);

    //assert
    expect(dom).toBeTruthy();
  });

  it("Should have 2 cards", async () => {
    //arrange
    configure({ testIdAttribute: "id" });
    const dom = render(<AppModulesList />);

    //act
    const cards = await dom.findAllByTestId("card-module");

    //assert
    expect(cards.length).toBe(2);
  });

  it("Should redirect when click on a card", async () => {
    //assert
    configure({ testIdAttribute: "id" });

    const history = createMemoryHistory();
    history.push("/");

    const dom = render(
      <Router history={history}>
        <Route path="/" exact component={AppModulesList} />
      </Router>
    );

    //act
    const card = await dom.findAllByTestId("card-module");
    fireEvent.click(card[0]);

    //assert
    expect(history.location.pathname).not.toBe("/");
  });
});
