import React from "react";
import { Chart, Interval, Tooltip } from "bizcharts";

//atoms
import Text, { TextVariant } from "components/atoms/Text/Index";

export interface VerticalBarGraphData {
  x: any;
  y: any;
}

interface VerticalBarGraphProps {
  data: VerticalBarGraphData[];
  graphTitle: string;
  height?: number;
}

function Index(props: VerticalBarGraphProps) {
  return (
    <div>
      <Text gutterBottom variant={TextVariant.H5}>
        {props.graphTitle}
      </Text>
      <Chart height={props.height ?? 300} autoFit data={props.data}>
        <Interval position="x*y" color="x" />
        <Tooltip shared />
      </Chart>
    </div>
  );
}

export default Index;
