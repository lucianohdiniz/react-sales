import React from "react";
import Typography from "@material-ui/core/Typography";

export enum TextVariant {
  H1 = "h1",
  H2 = "h2",
  H3 = "h3",
  H4 = "h4",
  H5 = "h5",
  H6 = "h6",
  SUBTITLE1 = "subtitle1",
  SUBTITLE2 = "subtitle2",
  BODY1 = "body1",
  BODY2 = "body2",
  CAPTION = "caption",
  BUTTON = "button",
  OVERLINE = "overline",
  SRONLY = "srOnly",
  INHERIT = "inherit",
}

interface TextProps {
  variant: TextVariant;
  gutterBottom?: boolean;
  children: React.ReactNode;
  onClick?: React.MouseEventHandler<any>;
  className?: string;
}

function TextComponent(props: TextProps) {
  return (
    <Typography
      className={props.className}
      onClick={props.onClick}
      variant={props.variant}
      gutterBottom={props.gutterBottom}
    >
      {props.children}
    </Typography>
  );
}

export default TextComponent;
