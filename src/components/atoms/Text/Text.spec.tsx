import React from "react";
import { render, configure } from "@testing-library/react";
import "@testing-library/jest-dom";

import Text, { TextVariant } from "./Index";

let text: string;

beforeEach(() => {
  text = "this is a text";
});

it("should create a text component", async () => {
  //arrange
  const dom = render(<Text variant={TextVariant.H2}>{text}</Text>);

  const textElement = await dom.findByText(text);

  //assert
  expect(dom).toBeTruthy();
  expect(textElement).toBeTruthy();
});
