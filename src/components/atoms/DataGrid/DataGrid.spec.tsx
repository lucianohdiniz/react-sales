import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom";

import DataGrid from "./Index";

let rowsMock, cols: Array<any>;

beforeEach(() => {
  rowsMock = [
    {
      id: 1,
      name: "test",
      email: "test@gmail.com",
    },
    {
      id: 2,
      name: "test2",
      email: "test2@gmail.com",
    },
  ];

  cols = [
    {
      field: "name",
    },
    {
      field: "email",
    },
  ];
});

it("should create an empty DataGrid", async () => {
  //arrange
  const dom = render(<DataGrid rows={[]} columns={[]} />);

  //act
  const noRows = await dom.findByText("No rows");

  //assert
  expect(dom).toBeTruthy();
  expect(noRows).toBeTruthy();
});

it("should create a DataGrid with some data", async () => {
  //arrange
  const dom = render(<DataGrid rows={rowsMock} columns={cols} />);

  //act
  const emailElement = await dom.findByText(rowsMock[0].email);

  //assert
  expect(emailElement).toBeTruthy();
});
