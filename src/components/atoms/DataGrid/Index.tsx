import React from "react";
import { GridColumns, DataGrid } from "@material-ui/data-grid";

interface DataGridProps {
  pageSize?: number;
  rows: any[];
  columns: GridColumns;
}

function DataGridComponent(props: DataGridProps) {
  return (
    <DataGrid
      disableSelectionOnClick
      rows={props.rows}
      columns={props.columns}
      pageSize={props.pageSize}
    />
  );
}

export default DataGridComponent;
