import React from "react";
import MatButton from "@material-ui/core/Button";
import { PropTypes } from "@material-ui/core";

interface ButtonProps {
  color?: PropTypes.Color;
  disabled?: boolean;
  id?: string;
  text: string;
  size?: "small" | "medium" | "large";
  variant?: "text" | "outlined" | "contained";
  onClick?: React.MouseEventHandler<HTMLElement> | undefined;
}

function ButtonComponent(props: ButtonProps) {
  return (
    <MatButton
      color={props.color}
      size={props.size}
      variant={props.variant}
      disabled={props.disabled}
      id={props.id}
      onClick={props.onClick}
    >
      {props.text}
    </MatButton>
  );
}

export default ButtonComponent;
