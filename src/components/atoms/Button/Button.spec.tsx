import React from "react";
import { render, fireEvent, configure } from "@testing-library/react";
import "@testing-library/jest-dom";

import Button from "./Index";

let text, id: string;

beforeEach(() => {
  text = "test button";
  id = "test_button_id";

  configure({ testIdAttribute: "id" });
});

it("should create button component", async () => {
  //arrange
  const dom = render(<Button text={text} />);

  //act
  const button = await dom.findByText(text);

  //assert
  expect(button).toBeTruthy();
});

it("should emit an event when the button is clicked", async () => {
  //arrange
  const onClickMock = jest.fn();
  onClickMock.mockReturnValue((event: MouseEvent) => console.log(event));

  const dom = render(<Button text={text} onClick={onClickMock} />);

  //act
  const button = await dom.findByText(text);
  fireEvent.click(button);

  //assert
  expect(onClickMock).toHaveBeenCalled();
});

it("should create a disabled button", async () => {
  //arrange
  const dom = render(<Button text={text} disabled={true} id={id} />);

  const button = await dom.findByTestId(id);

  //assert
  expect(button).toBeDisabled();
});
