import React, { ChangeEventHandler } from "react";
import TextField from "@material-ui/core/TextField";

interface InputProps {
  className?: string;
  type: string;
  label: string;
  required?: boolean;
  disabled?: boolean;
  id?: string;
  onChange?: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
}

function InputComponent(props: InputProps) {
  return (
    <TextField
      className={props.className}
      type={props.type}
      required={props.required}
      id={props.id}
      disabled={props.disabled}
      onChange={props.onChange}
      label={props.label}
      variant="outlined"
    />
  );
}

export default InputComponent;
