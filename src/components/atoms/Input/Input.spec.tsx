import React from "react";
import { render, configure, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import Input from "./Index";

let id: string;

beforeEach(() => {
  id = "test_input_id";
  configure({ testIdAttribute: "id" });
});

test("it should create Input", async () => {
  //arrange
  const dom = render(<Input label="test" type="text" />);

  //assert
  expect(dom).toBeTruthy();
});

test("it should be a disabled input", async () => {
  //arrange
  const dom = render(
    <Input label="test" type="text" disabled={true} id={id} />
  );

  //act
  const input = await dom.findByTestId(id);

  //assert
  expect(input).toBeDisabled();
});

test("it should emit an event on change", async () => {
  //arrange
  const onChangeMock = jest.fn();
  onChangeMock.mockReturnValue(() => {});

  const dom = render(
    <Input
      type="text"
      label="test"
      disabled={true}
      id={id}
      onChange={onChangeMock}
    />
  );

  //act
  const input = await dom.findByTestId(id);
  fireEvent.change(input, { target: { value: "naruto" } });

  //assert
  expect(onChangeMock).toHaveBeenCalled();
});
