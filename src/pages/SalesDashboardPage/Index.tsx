import React from "react";
//templates
import BaseTemplate from "components/templates/Base/Index";

//organisms
import Nav from "components/organisms/Nav/Index";
import SalesDashboard from "components/organisms/SalesDashboard/Index";

interface SalesDashboardPageProps {}

function SalesDashboardPage(props: SalesDashboardPageProps) {
  return (
    <BaseTemplate nav={<Nav />}>
      <SalesDashboard />
    </BaseTemplate>
  );
}

export default SalesDashboardPage;
