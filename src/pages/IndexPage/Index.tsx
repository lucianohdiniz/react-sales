import React from "react";

//templates
import BaseTemplate from "components/templates/Base/Index";

//organisms
import Nav from "components/organisms/Nav/Index";
import AppModulesList from "components/organisms/AppModulesList/Index";

interface IndexPageProps {}

function IndexPage(props: IndexPageProps) {
  return (
    <BaseTemplate nav={<Nav />}>
      <AppModulesList />
    </BaseTemplate>
  );
}

export default IndexPage;
