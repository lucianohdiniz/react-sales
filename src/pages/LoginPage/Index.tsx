import React from "react";
import EmptyTheme from "components/templates/Empty/Index";

//organisms
import LoginForm from "components/organisms/LoginForm/Index";

interface LoginPageProps {}

function Index(props: LoginPageProps) {
  return (
    <EmptyTheme>
      <LoginForm />
    </EmptyTheme>
  );
}

export default Index;
