import React from "react";

//templates
import BaseTemplate from "components/templates/Base/Index";

//organisms
import Nav from "components/organisms/Nav/Index";
import SalesList from "components/organisms/SalesList/Index";

interface SalesPageProps {}

function SalesPage(props: SalesPageProps) {
  return (
    <BaseTemplate nav={<Nav />}>
      <SalesList />
    </BaseTemplate>
  );
}

export default SalesPage;
