import React from "react";
import store from "../index";

import { signIn, signOut } from "./login.reducer";

let username = "test_user";

const stateSignIn = () => {
  store.dispatch(
    signIn({
      username,
    })
  );
};

const stateSignOut = () => {
  store.dispatch(signOut());
};

it("should loggedUser not be null after signIn", () => {
  //arrange
  stateSignIn();

  //assert
  const state = store.getState();
  expect(state.login.loggedUser.username).toBe(username);
});

it("should loggedUser be null after signOut", () => {
  //arrange
  stateSignIn();

  //act
  stateSignOut();

  //assert
  const state = store.getState();
  expect(state.login.loggedUser).toBeFalsy();
});
