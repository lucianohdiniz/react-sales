import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { User } from "../../models/User";

interface LoginState {
  loggedUser: User | null;
}

const initialState: LoginState = {
  loggedUser: null,
};
const user_key = "logged_user";

export const loginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    signIn(state: LoginState, action: PayloadAction<User>): void {
      const userJson = JSON.stringify(action.payload);
      localStorage.setItem(user_key, userJson);

      state.loggedUser = action.payload;
    },
    signOut(state: LoginState): void {
      state.loggedUser = null;
      localStorage.removeItem(user_key);
    },
  },
});

export { user_key };
export const { signIn, signOut } = loginSlice.actions;
export default loginSlice.reducer;
