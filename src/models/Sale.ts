export interface Sale {
  id: number;
  quantityordered: number;
  priceeach: number;
  orderlinenumber: number;
  sales: number;
  orderdate: string;
  status: string;
  qtR_ID: number;
  montH_ID: number;
  yeaR_ID: number;
  productline: string;
  msrp: number;
  productcode: string;
  customername: string;
  phone: string;
  addresslinE1: string;
  addresslinE2: string;
  city: string;
  state: string;
  postalcode: string;
  country: string;
  territory: string;
  contactlastname: string;
  contactfirstname: string;
  dealsize: string;
}
