export interface User {
  username: string;
}

export interface LoginUser extends User {
  password: string;
}
