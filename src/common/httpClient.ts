import axios from "axios";

const client = axios.create({
  baseURL: "http://localhost:3030",
});

client.interceptors.response.use((res) => {
  return res.data;
});

export default client;
