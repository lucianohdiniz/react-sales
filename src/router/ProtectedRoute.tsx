import React from "react";
import { RouteProps, Redirect, Route } from "react-router-dom";

//redux
import { signIn, user_key } from "store/login/login.reducer";
import { useAppSelector, useAppDispatch } from "hooks";

//models
import { User } from "models/User";

function ProtectedRoute(props: RouteProps) {
  const loggedUser = useAppSelector((state) => state.login.loggedUser);
  const dispatch = useAppDispatch();

  if (!loggedUser) {
    const storageUser = localStorage.getItem(user_key);

    if (!storageUser) {
      return <Redirect to="/login" />;
    } else {
      const user = JSON.parse(storageUser) as User;
      dispatch(signIn(user));
    }
  }

  return <Route {...props} />;
}

export default ProtectedRoute;
