import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import ProtectedRoute from "./ProtectedRoute";
import { routes } from "./routes";

const AppRoutes = () => (
  <BrowserRouter>
    <Switch>
      {routes.map((route, index) =>
        route.requireAuth ? (
          <ProtectedRoute
            key={index}
            exact
            path={route.path}
            component={route.component}
          />
        ) : (
          <Route
            key={index}
            exact
            path={route.path}
            component={route.component}
          />
        )
      )}
    </Switch>
  </BrowserRouter>
);

export default AppRoutes;
