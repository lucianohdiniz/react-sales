import React from "react";
import { RouteComponentProps } from "react-router-dom";
//pages

import IndexPage from "pages/IndexPage/Index";
import LoginPage from "pages/LoginPage/Index";
import SalesPage from "pages/SalesPage/Index";
import SalesDashboardPage from "pages/SalesDashboardPage/Index";

interface AppRoute {
  path: string;
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>
    | undefined;
  requireAuth: boolean;
}

const routes: Array<AppRoute> = [
  {
    component: IndexPage,
    path: "/",
    requireAuth: true,
  },
  {
    component: LoginPage,
    path: "/login",
    requireAuth: false,
  },
  {
    component: SalesPage,
    path: "/sales",
    requireAuth: true,
  },
  {
    component: SalesDashboardPage,
    path: "/sales/dashboard",
    requireAuth: true,
  },
];

export { routes };
