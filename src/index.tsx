import React from "react";
import ReactDOM from "react-dom";
import AppRoutes from "router/AppRoutes";

import { Provider } from "react-redux";
import store from "store";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <AppRoutes />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
