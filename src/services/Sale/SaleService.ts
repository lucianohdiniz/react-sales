import client from "../../common/httpClient";
import { Sale } from "../../models/Sale";

interface SaleService {
  getAll(): Promise<Array<Sale>>;
}

export const getAll = (): Promise<Array<Sale>> => client.get("/sales");

const saleService: SaleService = {
  getAll,
};

export default saleService;
