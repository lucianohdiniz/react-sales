import saleService from "./SaleService";

it("Should return a list of sales", async () => {
  const salesMock = [
    {
      id: 10107,
      quantityordered: 30,
      priceeach: 95.7,
      orderlinenumber: 2,
      sales: 2871,
      orderdate: "2/24/2003 0:00",
      status: "Shipped",
      qtR_ID: 1,
      montH_ID: 2,
      yeaR_ID: 2003,
      productline: "Motorcycles",
      msrp: 95,
      productcode: "S10_1678",
      customername: "Land of Toys Inc.",
      phone: "2125557818",
      addresslinE1: "897 Long Airport Avenue",
      addresslinE2: "",
      city: "NYC",
      state: "NY",
      postalcode: "10022",
      country: "USA",
      territory: "NA",
      contactlastname: "Yu",
      contactfirstname: "Kwai",
      dealsize: "Small",
    },
  ];

  const saleServiceMock = jest.spyOn(saleService, "getAll");
  saleServiceMock.mockReturnValueOnce(Promise.resolve(salesMock));

  const result = await saleService.getAll();
  expect(result).toBe(salesMock);
});
