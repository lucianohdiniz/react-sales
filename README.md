# Starting the project

first you need to run the api with the following command:

### `yarn api`

after that just run in a new terminal:

### `yarn start`

## Cypress

To open cypress console and run your features just run:

### `yarn cypress`
