Feature: Sales login

    Scenario: Login
        Given access the sales login page
        When  type my correct username and password
        And click login button
        Then it should authenticate and redirect to index page
        
    Scenario: Incorrect username and password
        Given access the sales login page
        When type incorrent username and password
        And click login button
        Then it should emit an alert
