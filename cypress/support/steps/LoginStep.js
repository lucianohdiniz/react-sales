/* global Scenario, Given, And, Then, When */

import LoginPage from "../pageobjects/LoginPage";
const loginPage = new LoginPage();

Given("access the sales login page", () => {
  loginPage.accessSite();
});

When("type my correct username and password", () => {
  loginPage.typeUsername("admin");
  loginPage.typePassword("admin");
});

And("click login button", () => {
  loginPage.clickLoginButton();
});

Then("it should authenticate and redirect to index page", () => {
  loginPage.checkIfLoginWasSuccessful();
});
