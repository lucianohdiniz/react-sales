/* global Scenario, Given, And, Then, When */

import LoginPage from "../pageobjects/LoginPage";
const loginPage = new LoginPage();

Given("access the sales login page", () => {
  loginPage.accessSite();
});

When("type incorrent username and password", () => {
  loginPage.typeUsername("wrong");
  loginPage.typePassword("credentials");
});

And("click login button", () => {
  loginPage.observeAlert();
  loginPage.clickLoginButton();
});

Then("it should emit an alert", () => {
  loginPage.checkIfAlertWasTriggered();
});
