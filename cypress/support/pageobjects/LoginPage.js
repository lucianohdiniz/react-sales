/// <reference types="Cypress" />

const url = Cypress.config("baseUrl");

export default class LoginPage {
  constructor() {
    this.alertTriggered = false;
  }

  accessSite = () => {
    cy.visit(url);
  };

  typeUsername = (username) => {
    cy.get("#username").type(username);
  };

  typePassword = (password) => {
    cy.get("#password").type(password);
  };

  clickLoginButton = () => {
    cy.get("#login_btn").click();
  };

  checkIfLoginWasSuccessful = () => {
    cy.get("#card-module", { timeout: 10000 }).should("not.be.undefined");
  };

  observeAlert = () => {
    cy.on("window:alert", () => {
      this.alertTriggered = true;
    });
  };

  checkIfAlertWasTriggered = () => {
    expect(this.alertTriggered).to.be.equal(true);
  };
}
